#!/bin/bash

for p in tests/asciimath-*.plantuml; do
    echo "processing ${p}..."
    java -jar /opt/plantuml/plantuml.jar "$p"
done
