#!/bin/bash
#
# Kiet To
#
# Add this to crontab
#    0 12 * * * cd <fullpath> && ./check-for-updates.sh
#

MAINTAINER="kwrazi@gmail.com"
# PKGVER=$(grep pkgver= PKGBUILD | cut -f2 -d=)
PKGVER=$(w3m -dump "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=plantuml" | grep pkgver= | cut -f2 -d=)

#LATEST=$(w3m -dump "https://sourceforge.net/projects/plantuml/files/" | sed -rn 's/plantuml-([0-9]+).tar.gz/\1/p' | head -n 1 | gawk '{print $1;}')
LATEST=$(curl -s "https://sourceforge.net/p/plantuml/activity/feed?source=project_activity" | sed -e 's/>/>\n/g' | sed -ne 's/^.*plantuml-\([0-9\.]*\).tar.gz.*$/\1/p' | head -n1)

if [ "${PKGVER}" == "${LATEST}" ]; then
    MESG="PKGBUILD is up to date (pkgver=${LATEST}) with sourceforge. No action."
    echo "${MESG}"
    # echo "${MESG}" | mail -s "plantuml is up-to-date." "${MAINTAINER}"
else
    MESG="PKGBUILD (${PKGVER}) is different from sourceforge (${LATEST}). Please update PKGBUILD."
    echo "${MESG}"
    # echo "${MESG}" | mail -s "plantuml aur needs updating." "${MAINTAINER}"
fi
