Note for maintainers
--------------------

This is the environment that I use to maintain the AUR plantuml packages.

```
# test build package
makepkg -sC
```

```
# clean build files
makepkg -c
```

```
# make .SRCINFO file
makepkg --printsrcinfo > .SRCINFO

```
