#!/bin/bash
# Kiet To

function clean () {
    rm -rfv src
    rm -rfv pkg
    rm -fv plantuml-*-any.pkg.tar.xz
    rm -fv plantuml-*.tar.gz
}

function test_executable() {
    hash compare || yaourt -Sy --noconfirm --needed imagemagick
    rm -fv tests/*.png
    PLANTUML=$(find ./plantuml/pkg -name "plantuml.jar")
    if [ ! -f "${PLANTUML}" ]; then
        pushd ./plantuml;
        makepkg -s;
        popd
    fi
    echo "Using ${PLANTUML}..."
    SUM=0
    WORKDIR=./tests
    [ ! -d ${WORKDIR} ] && mkdir -pv ${WORKDIR}
    for test in ${WORKDIR}/*.plantuml; do
        echo "   ...processing ${test}"
        java -jar "${PLANTUML}" ${test}
        IMGFILE=$(basename "${test}" .plantuml).png
        EXPECTED=expected/${IMGFILE}
        RMSE=$(compare -metric RMSE "${WORKDIR}/${IMGFILE}" "${EXPECTED}" null: |& cut -f1 -d' ')
        # if compare -metric RMSE "${WORKDIR}/${IMGFILE}" "${EXPECTED}" null: >& /dev/null; then
        # else
        #     echo "image comparison failed."
        # fi
        echo "sum = ${SUM} + ${RMSE}"
        SUM=$(echo "${SUM} + ${RMSE}" | bc)
    done
    echo "---------------"
    pushd ./plantuml/
    makepkg --printsrcinfo > .SRCINFO
    popd
    echo "---------------"
    java -jar "${PLANTUML}" -version

    echo "**** Total difference: ${SUM}"
}

function update_test() {
    ls tests/*.png &> /dev/null || test_executable
    mv -fv tests/*.png expected/
}

case "$1" in
    "update")
        update_test
        ;;
    "test")
        test_executable
        ;;
    "clean")
        clean
        ;;
    *)
        cat <<EOF
Syntax:
  $0 [update | test | clean]
EOF
        ;;
esac
